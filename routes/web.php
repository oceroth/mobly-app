<?php

use App\Product as Product;
use Illuminate\Http\Request as Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/categories', 'CategoryController@index');
Route::get('/categories/new', 'CategoryController@create');
Route::post('/categories/store', 'CategoryController@store');
Route::get('/categories/edit/{category}', 'CategoryController@edit');
Route::post('/categories/update/{category}', 'CategoryController@update');
Route::delete('/categories/{category}', 'CategoryController@destroy');

Route::resource('products', 'ProductController');

Route::get('/', 'ShopController@index')->name('home')->middleware('initCart');
Route::get('/cart', 'ShopController@cart')->middleware('initCart');
Route::put('/cart/{productId}', 'ShopController@addToCart')->middleware('initCart');
Route::delete('/cart/{productId}', 'ShopController@removeFromCart')->middleware('initCart');
Route::get('/c/{category}', 'ShopController@category')->middleware('initCart');
Route::get('/p/{product}', 'ShopController@product')->middleware('initCart');
Route::post('/checkout', 'ShopController@checkout');
Route::get('/order', function(){

	$orders = App\Order::with(['buyer','address','products'])->get();
	return view('orders.index',compact('orders'));
	//return response()->json($orders);
});
Route::post('/search', function(Request $request){
	$query = $request->input('query');
	$results = Product::search($query);
	// return response()->json($products);
	return view('shop.search',compact('results','query'));
})->name('search');
