<?php

use Faker\Generator as Faker;

$factory->define(App\Buyer::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name,
        'cpf' => $faker->cpf(false),
        'order_id' => 0 //we will overwrite this in the seeder class
    ];
});
