<?php

use Faker\Generator as Faker;



$factory->define(App\ShipAdress::class, function (Faker $faker) {
	$bairros = [
		'Água Rasa',
		'Alto da Mooca',
		'Chácara Mafalda',
		'Chácara Paraíso',
		'Jardim Guanabara',
		'Jardim Haddad',
		'Jardim Itália',
		'Jardim Silveira',
		'Vila Bertioga',
		'Vila Celeste',
		'Vila Cláudia',
		'Vila Clotilde',
		'Vila Diva',
		'Vila Graciosa',
		'Vila Invernada',
		'Vila Leme',
		'Vila Libanesa',
		'Vila Lúcia Elvira',
		'Vila Oratório',
		'Vila Paulina',
		'Vila Regente Feijó',
		'Vila Rio Branco',
		'Vila Santa Clara'
	];
    return [
        //
		'address' => $faker->streetName(),
		'secondary_address' => null,
		'number' => rand(1,9999),
		'neighborhood' => $bairros[rand(0,count($bairros)-1)],
		'city' => $faker->city(),
		'state' => $faker->state(),
		'postcode' => $faker->postcode(),
		'order_id' => 0 //we will overwrite this in the seeder class

    ];
});
