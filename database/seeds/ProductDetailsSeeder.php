<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class ProductDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $products = array(1,2,3,4,5,6,7,8);
        $details = array(1,2,3);
        $values = [
	        array('Sierra', 'Tok & Stok', 'Aramóveis', 'Biemme'),
	        array('Marrom', 'Branco', 'Cinza', 'Preto'),
	        array('Madeira', 'Alumínio', 'Compensado MDF')
        ];
        foreach ($products as $product) {
        	foreach ($details as $detail) {
        	 	$data['product_id'] = $product;
        	 	$data['detail_id'] = $detail;
        	 	$data['value'] = $values[$detail-1][rand(0, count($values[$detail-1]) -1)];
                $data['created_at'] = Carbon::now();
                $data['updated_at'] = Carbon::now();

	        	DB::table('product_details')->insert($data);
        	 } 
        }
    }
}
