<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class DetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $samples = [
            'marca',
            'cor',
            'material'
        ];
        foreach ($samples as $sample) {
            DB::table('details')->insert([
                'name' => $sample,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
