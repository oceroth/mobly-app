<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class ProductCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = array(
        	1 => [1,2,3,],
        	5 => [4,5],
        	4 => [6,7,8],
        	15 =>[1,3,5,6,7]
        );
        
        foreach ($categories as $category_id => $products) {
        	foreach ($products as $product_id) {
        	 	$data['product_id'] = $product_id;
                $data['category_id'] = $category_id;
                $data['created_at'] = Carbon::now();
                $data['updated_at'] = Carbon::now();
	        	DB::table('product_categories')->insert($data);
        	 } 
        }
    }
}
