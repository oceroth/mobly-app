<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class OrderItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Items shouldn't repeat in the same Order.
        $fakeOrders = range(1,6);
        foreach ($fakeOrders as $order_id) {
        	$numberOfItems = rand(1,4);
        	$fakeProducts = range(1,8);
        	for ($i = 0; $i < $numberOfItems ; $i++) {
        		shuffle($fakeProducts); 
	        	DB::table('order_items')->insert([
	        		'product_id' => array_pop($fakeProducts),
	        		'order_id' => $order_id,
	        		'quantity' => rand(1,3),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
	        	]);
        	}
        }
    }
}
