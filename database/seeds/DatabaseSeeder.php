<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(DetailsSeeder::class);
        $this->call(ProductsSeeder::class);
        $this->call(ProductDetailsSeeder::class);
        $this->call(ProductCategoriesSeeder::class);
        $this->call(OrdersSeeder::class);
        $this->call(OrderItemsSeeder::class);
        $this->call(ShipAddressesSeeder::class);
        $this->call(BuyersSeeder::class);
    }
}
