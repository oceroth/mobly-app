<?php

use Illuminate\Database\Seeder;

class ShipAddressesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $fakeOrders = range(1,6);
        foreach ($fakeOrders as $order_id) {
            factory(App\ShipAdress::class)->create([
                'order_id' => $order_id,
            ]);
        }
    }
}
