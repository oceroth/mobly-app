<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon as Carbon;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $samples = [
            'sofá',
            'rack, estante e painel',
            'sala de jantar',
            'poltrona',
            'cadeira',
            'banco e banqueta',
            'guarda-roupa',
            'colchão',
            'cama box',
            'cama',
            'cabeceira, calçadeira e baú',
            'quarto completo',
            'cozinha compacta',
            'cozinha modulada',
            'produtos em liquidação'
        ];
        foreach ($samples as $sample) {
            DB::table('categories')->insert([
                'name' => $sample,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
