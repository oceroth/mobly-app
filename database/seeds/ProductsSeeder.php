<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $samples = [
        	[
        		'name'=>'Sofá 4 Lugares Net Confort Assento Retrátil e Reclinável Grafite',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>999.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()

        	],
        	[
        		'name'=>'Sofá 4 Lugares Net Confort Assento Retrátil e Reclinável Marrom Claro',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>899.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        	],
        	[
        		'name'=>'Sofá 4 Lugares Connect Retrátil e Reclinável Suede Amassado Cinza',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>799.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        	],
        	[
        		'name'=>'Kit 6 Poltronas Executiva 1 Lugar Corino Pé Aberto Café',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>1020.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        	],
        	[
        		'name'=>'Poltrona Duda Siena Choco/Passion',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>209.90,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        	],
        	[
        		'name'=>'Cadeira Charles Eames Eiffel Em Polipropileno Preta Sem Braços, Base Em Madeira',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>97.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        	],
        	[
        		'name'=>'Cadeira Presidente Pel-8009 Giratória Regulagem De Altura A Gás Tela Mesh Preta - Pelegrin',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>295.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        	],
        	[
        		'name'=>'Conjunto 4 Cadeiras Charles Eames Eiffel Wood Base Madeira',
        		'description'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi perspiciatis ipsam officiis error accusantium ab quos aspernatur ullam, dolore, sapiente molestiae molestias dolores. Vitae totam, voluptatum libero dicta, quam architecto!',
        		'image'=>'https://picsum.photos/200/300/?random',
        		'price'=>440.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        	]
        ];
        foreach ($samples as $sample) {
            DB::table('products')->insert([
                'name' => $sample['name'],
                'description' => $sample['description'],
                'image' => $sample['image'],
                'price' => $sample['price'],
            ]);
        }
    }
}
