<?php

use Illuminate\Database\Seeder;

class BuyersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $fakeOrders = range(1,6);
        foreach ($fakeOrders as $order_id) {
	        factory(App\Buyer::class)->create([
	        	'order_id' => $order_id,
	        ]);
        }
    }
}
