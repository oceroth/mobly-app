
## Mobly-App

Uma loja virtual simples, desenvolvida com Laravel 5.7, laradock e vue.js.
Apenas um teste. Trabalho em andamento.

## Setup

O projeto usa o laradock como depenência. O Laradock traz uma configuração pronta para ser personalizada para uso nodesenvolvimento de várias aplicações especialmente Laravel PHP.

O seu sistema precisa atender aos requisitos básicos do Laradock, no geral, os requisitos básicos para usar o docker e docker-compose. Se você já utiliza esses programas, não deve ter maiores dificuldades para instalar. A configuração no Windows pode ser um pouco mais problemática.
Leia as instruções de instalação e configuração inicial do laradock no site:

laradock.io
http://laradock.io/introduction/

no Github:
https://github.com/laradock/laradock

O modo convencional de instalar o laradock(para rodar um único projeto), é abrir a pasta em que você costuma criar seus projetos, algo como  '~/projects' e clonar o laradock e o projeto laravel para o mesmo diretório que você vai criar.

assim a pasta 'mobly-app' e 'laradock' seriam 'vizinhas' na mesma pasta. 

Para rodar o mobly-app é preciso inicializar os serviços dependentes com o docker-composer com o comando:

docker-compose up nginx mysql redis

De dentro do diretório do laradock, onde está o docker-compose.yml.
Para rodar as migrations e construir e carregar o banco de dados com os dados de teste é necessário rodar esses comandos de dentro do container workspace, que é carregado como dependência de todos os outros serviços. Use o para executar comandos do artisan, composer, npm...

docker-compose exec --user=laradock workspace

Instale as dependências do PHP com o composer install e as do npm também, dentro da pasta mobly-app

Eu tive alguns problemas para rodar a imagem mysql, os arquivos que ela criava tinham permissão negada mesmo para o usuário root.  Precisei desativar o SELinux do meu sistema (Fedora 28), para resolver o problema.

E também outro problema na hora da imagem do mysql criar o usuário e banco de dados do mysql. precisei logar no container como root resetar todos os usuários e bancos e personalizar o arquivo de configuração na pasta mysql, dentro da pasta do laradock, para recriar o banco e o usuário padrão.

Também foi preciso claro usar essas informações para configurar minhas conexões de banco de dados no arquivo .env do laravel me 'mobly-app'

Depois de iniciar o ambiente com o docker-compose:

docker-compose up nginx mysql redis

A aplicação estára acessível em http://localhost/

