@extends("shared.shop-layout")
@section('head')
	@component('shared.components.title')
		checkout
	@endcomponent
@endsection

@section('content')
<div class="row justify-content-around">
	<div class="card my-4 p-2 col-4">
		<h4 class="title">Seus itens</h4>
		<div class="mb-4">
		    <ul class="list-group mb-3">
		    	@foreach($toBeOrdered as $product)
				    <li class="list-group-item d-flex justify-content-between lh-condensed">
				        <h5 >
				        	<span class="badge badge-secondary badge-pill">
				        		{{session('cartItems')[$product->id]}}
				        	</span>
				        	<a href="{{ action('ShopController@product',['product' => $product->id]) }}">{{ $product->name }}</a>
				        </h5>
				    </li>
		    	@endforeach
			    <li class="list-group-item d-flex justify-content-between">
			        <span>Total (BRL)</span>
			        <strong>$ {{$total}}</strong>
			    </li>
			</ul>
		</div>
	</div>
	<div class="card my-4 p-2 col-7">
		<h4 class="title">Finalizar pedido</h4>
		<form action="{{ action('ShopController@checkout') }}" method="post" name="checkout">
			@csrf
			@if(count($errors))
				<div class="alert alert-danger">
					<ul>
						@foreach($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<div class="row">
				<div class="col-md-6 mb-3">
					<label for="name" class="text-capitalize">nome</label>
					<input class="form-control" type="text"  name="name">
				</div>
				<div class="col-md-6 mb-3">
					<label for="cpf" class="text-uppercase">cpf</label>
					<input class="form-control" type="text"  name="cpf">
				</div>
			</div>
			<div class="row">
				<div class="col mb-3">
					<label for="address" class="text-capitalize">Rua</label>
					<input class="form-control" type="text"  name="address">
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mb-3">
					<label for="number" class="text-capitalize">número</label>
					<input class="form-control" type="text"  name="number">
				</div>
				<div class="col-md-4 mb-3">
					<label for="secondary_address" class="text-capitalize">complemento</label>
					<input class="form-control" type="text"  name="secondary_address">
				</div>
				<div class="col-md-4 mb-3">
					<label for="neighborhood" class="text-capitalize">bairro</label>
					<input class="form-control" type="text"  name="neighborhood">
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 mb-3">
					<label for="city" class="text-capitalize">cidade</label>
					<input class="form-control" type="text"  name="city">
				</div>
				<div class="col-md-3 mb-3">
					<label for="state" class="text-capitalize">estado</label>
					<input class="form-control" type="text"  name="state">
				</div>
				<div class="col-md-3 mb-3">
					<label for="postcode" class="text-uppercase">cep</label>
					<input class="form-control" type="text"  name="postcode">
				</div>
			</div>
			<div class="row justify-content-center">
				<button type="submit" class="btn btn-success btn-lg d-flex ">comprar</button>
			</div>
		</form>
	</div>
</div>	
@endsection