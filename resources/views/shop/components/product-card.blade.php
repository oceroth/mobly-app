<div class="col-md-4">
    <div class="card mb-4 box-shadow">
        <img class="card-img-top" src="https://picsum.photos/750/475?random" alt="Card image cap">
        <div class="card-body">
            <p class="card-text">{{$title}}.</p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    <a href="{{$primaryAction}}" class="btn btn-sm btn-outline-secondary">{{$primaryLabel}}</a>
                    <form method="POST" name="cart" action="{{$secondaryAction}}">
                        @csrf
                        @method('put')
                        <input type="hidden" name="quantity" value="1">
                    <button type="submit" class="btn btn-sm btn-outline-secondary">{{$secondaryLabel}}</button>
                    </form>
                </div>
                <span class="h4 text-muted"><small>R$</small> {{$price}}</span>
            </div>
        </div>
    </div>
</div>
