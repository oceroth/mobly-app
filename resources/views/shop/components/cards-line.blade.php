<div class="row pt-4">
    <div class="col-12">
        <h3 class="text-muted text-uppercase">{{$slot}}</h3>
    </div>
</div>
<div class="row">
    @foreach($items as $item)
        @component('shop.components.product-card')
            @slot('title')
                {{$item->name}}
            @endslot

            @slot('primaryLabel')
                🔎 Detalhes
            @endslot

            @slot('primaryAction')
                {{ action('ShopController@product', ['product' => $item->id, 'name' => $item->name]) }}
            @endslot

            @slot('secondaryLabel')
                ➕ carrinho
            @endslot
            
            @slot('secondaryAction')
                {{ action('ShopController@addToCart', ['id' => $item->id]) }}
            @endslot

            @slot('price')
                {{$item->price}}
            @endslot
        @endcomponent
    @endforeach
</div>
  