@extends('shared.shop-layout')

@section('head')
	@component('shared.components.title')
	    Resultados da busca
    @endcomponent
@endsection

@section('content')
	
	@component('shop.components.cards-line',['items' => $results])
		Resultados da sua busca por "{{$query}}" ({{count($results)}})
	@endcomponent

@endsection