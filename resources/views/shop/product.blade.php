@extends('shared.shop-layout')

@section('head')
	@component('shared.components.title')
		{{$product->name}}
	@endcomponent
@endsection

@section('content')
<div class="row justify-content-around">
	<div class="card col-3 mt-4 px-0">
		<img src="{{$product->image}}" alt="" class="card-img-top">
		<div class="card-body">
			<h4 class="card-title">{{$product->name}}</h4>
			<hr>
			<h5 class="card-subtitle text-center text-muted"><small>R$ </small><strong>{{$product->price}}</strong></h5>
		</div>
	</div> 
	<div class="card col-5 mt-4 px-0">
		<div class="card-body">
			<h5 class="card-title text-uppercase">Detalhes do produto</h5>
			<hr>
			<p class="lead">{{$product->description}}</strong></p>
			<h4 class="card-subtitle pt-4">características</h4>
			<hr>
			<dl class="border px-1">
				@foreach($product->details as $detail)
					<dt class="bg-light text-capitalize">{{$detail->name}}: </dt>
					<dd class="text-capitalize" >{{$detail->pivot->value}}</dd>
				@endforeach
			</dl>
		</div>
	</div>
	<div class="card col-3 mt-4 px-0">
		<div class="card-body text-center">
			<h5 class="card-title text-uppercase">comprar</h5>
			<hr>
			<!-- if the prodduct isn't in hte cart -->
			@if(!collect(session('cartItems'))->has($product->id))
				<form action="{{action('ShopController@addToCart',['productId'=>$product->id])}}" method="POST" name="cart">
					@csrf
					@method('put')
					<div class="input-group mb-3">
						<input type="number" name="quantity" id="quantity" min='0' value='1' class="form-control">
					</div>
					<button type="submit" class="btn btn-primary">Adicionar ao carrinho</button>
				</form>
			@else
				<form action="{{action('ShopController@removeFromCart',['productId'=>$product->id])}}" method="POST" name="cart">
					@csrf
					@method('delete')
					<span style="yellow">👍</span><p>você já tem este item no carrinho. Deseja finalizar sua compra?</p>
					<a href="{{ action('ShopController@cart') }}" class="btn btn-success">Finalizar pedido</a>
					<p>Ou você pode remover itens, você tem {{ collect(session('cartItems'))->get($product->id) }} unidade no seu carrinho:</p>
					<div class="input-group mb-3">
						<input type="number" name="quantity" id="quantity" min='0' value='1' class="form-control">
					</div>
					<button type="submit" class="btn btn-primary btn-danger">remover do carrinho</button>
				</form>
			@endif
		</div>
	</div>
	
</div>
@endsection
