@extends('shared.shop-layout')

@section('head')
	@component('shared.components.title')
	    Produtos
    @endcomponent
@endsection

@section('search')
	@component('shared.components.search')
	@endcomponent
@endsection

@section('content')
	
	@component('shop.components.cards-line',['items' => $promo1])
		Sofás incríveis para a sua sala de estar!
	@endcomponent

	@component('shop.components.cards-line', ['items' => $promo2])
		Aproveite os preços desses items!
	@endcomponent

@endsection