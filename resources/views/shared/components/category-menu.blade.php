<div id="category-menu">
	<p class="h3 text-uppercase">todas as categorias</p>
	@foreach($menuItems as $id => $name)
		<a href="{{ action('ShopController@category', ['category' => $id, 'name' => $name]) }}">{{$name}} | </a>
	@endforeach

</div>