<div class="row flex-row py-3 justify-content-center">
	<div class="col-md-8 d-flex">
		<form action="{{ route('search',['query' => null]) }}" method="post">
			<div class="input-group">
				<input type="text" placeholder="🔎 Busca" name="query" class="form-control">
				<div class="input-group-append">
					<button class="btn btn-primary" type="submit">
						Buscar
					</button>
				</div>
			</div>
			@csrf
			@method('post')
			
		</form>
	</div>
</div>