<div id="cart-snippet">
	<p class="h3 text-uppercase">Carrinho</p>
	@if(session()->has('cartItems') && count(session('cartItems')) > 0)
	<p>você tem {{ collect(session('cartItems'))->sum() }} items no carrinho</p>
	<a href="{{action('ShopController@cart')}}" class="btn btn-primary text-uppercase">ver carrinho</a>
	@else
	<p>você ainda não tem nenhum item no seu carrinho</p>
	@endif
</div>