<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                min-height: 100vh;
                margin: 0;
            }

            a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .full-height {
                min-height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > p {
                display: block;
            }


            .m-b-md {
                margin-bottom: 30px;
            }
            .btn {
                text-transform: uppercase;
            }
            .form {
                text-transform: uppercase;
                margin-bottom: 1em;
            }
            .danger {
                color: red;
            }
            .floating-bar a {
                font-size: 1.24rem;
            }
            .messages {
                border-width: 1px;
                border-style: solid;
                border-color: red;
                margin-bottom: 1em;
            }
        </style>
    </head>
    <body>
        <div id="app">
            @yield('content')
        </div>
    </body>
    <script src="/js/app.js"></script>
</html>
