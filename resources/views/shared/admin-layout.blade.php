<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">
    <!-- define in the 'head' section title and description components for every view -->
    @yield('head')

  </head>
  <body>
  	<header>
	  	<div class="navbar navbar-dark bg-dark" role='navigation'>
	  		<div class="container">
	  			<a href="{{route('home')}}" class="navbar-brand">MOBLY</a>
	  		</div>
	  	</div>
  	</header>
  	<main>
		<div class="container" id="app">
			@yield('content')
		</div>
  	</main>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/JavaScript" src="/js/app.js" ></script>
  </body>
</html>