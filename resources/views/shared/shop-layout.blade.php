<!doctype html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">
    <!-- define in the 'head' section title and description components for every view -->
    @yield('head')

  </head>
  <body>
  	<header>
	  	<div class="div text-white collapse bg-dark" id="navbar-header">
	  		<div class="container">
	  			<div class="row">
	  				<div class="col-sm-8 mt-3 ">
						@component('shared.components.category-menu')
						@endcomponent
	  				</div>
	  				<div class="col-sm-4 border-left border-white mt-3">
	  					@component('shared.components.cart-snippet')
	  					@endcomponent
					</div>
	  			</div>
	  		</div>
	  	</div>
	  	<div class="navbar navbar-dark bg-dark" role='navigation'>
	  		<div class="container">
	  			<a href="{{route('home')}}" class="navbar-brand">MOBLY</a>
				<button class="nav-toggler btn btn-secondary" type="button" data-toggle="collapse" data-target="#navbar-header" aria-role="navigation" >
					MENU
				</button>
	  		</div>
	  	</div>
  	</header>
  	<main>
		<div class="container" id="app">
			@yield('search')
			@yield('content')
		</div>
  	</main>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/JavaScript" src="/js/app.js" ></script>
  </body>
</html>