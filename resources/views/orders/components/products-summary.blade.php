@foreach($products as $product)
	<dl class="row">
		<dt class="col-2">{{$product->pivot->quantity}} x </dt>
		<dd class="col-10">{{$product->name}}</dd>
	</dl>
@endforeach
<dl class="row">
	<dt class="col-2">subtotal</dt>
	<dd class="col-10">{{money_format('%i',$products->sum('price'))}}</dd>
</dl>