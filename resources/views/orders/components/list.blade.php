<!-- orders/components/list @param App\Order $orders[] -->
<div class="container-fluid">
    <h3 class="my-3">{{$slot}}</h3>
    <table class="table table-striped">
        <thead>
            <tr class="text-capitalize">
                <th scope="col">#id</th>
                <th scope="col">comprador</th>
                <th scope="col">endereço</th>
                <th scope="col">produtos</th>
                <th scope="col">pendente?</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>
                        @component('orders.components.buyer-summary',['buyer' => $order->buyer])
                        @endcomponent
                    </td>
                    <td>
                        @component('orders.components.address-summary',['address'=>$order->address])
                        @endcomponent
                    </td>
                    <td>
                        @component('orders.components.products-summary',['products'=>$order->products])
                        @endcomponent
                    </td>
                    <td>{{$order->pending ? 'sim' : 'não'}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>