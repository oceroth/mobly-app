@extends('shared.admin-layout')

@section('head')
	@component('shared.components.title')
		Pedidos
	@endcomponent
@endsection

@section('content')
	<div class="container">
		@component('orders.components.list', ['orders' => $orders])
			Pedidos
		@endcomponent
	</div>
@endsection