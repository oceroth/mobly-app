@extends('shared.layout')
@section('content')
<div class="flex-center position-ref full-height">


    <div class="content">
        <div class="title m-b-md">
            Adicionar Produto
        </div>

        <div class="form">
            @component('products.components.form', ['product'=>$product])
            
                @slot('http_method')
                    POST
                @endslot

                @slot('submit_value')
                    adicionar
                @endslot

                @slot('action')
                    /products
                @endslot
            @endcomponent
        </div>
        <a href="{{ url()->previous() }}">voltar</a>
    </div>
</div>
@endsection