<form action="{{$action}}" method="post" >
	@if($errors->isNotEmpty())
		<div class="messages danger">
			@foreach($errors->all() as $error)
				<p>{{$error}}</p>
			@endforeach
		</div>
	@endif

	{{$slot}}

	@csrf
	<input type="hidden" name="_method" value="{{$http_method}}">
	<div>
		<label for="name">Nome</label>
		<input type="text" size="40" name="name" value="{{ old('name', $product->name) }}" required="true">
	</div>
	<div>
		<label for="descrição" value={{ old('name', $product->name) }}>Descrição</label>
		<textarea name="description" value="{{ old('description', $product->description) }}"></textarea>
	</div>
	<div>
		<label for="price">Preço</label>
		<input type="text" name="name" value="{{ old('price', $product->price) }}" required="true">
	</div>
	<div>
		<label for="image">Imagem</label>
		<input type="text" name="name" value="{{ old('image', $product->image) }}" required="true">
	</div>

	<input class="btn btn-submit"  type="submit" value="{{$submit_value}}">
</form>
