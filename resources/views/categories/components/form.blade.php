<form action="{{$action}}" method="post" >
	@if($errors->isNotEmpty())
		<div class="messages danger">
			@foreach($errors->all() as $error)
				<p>{{$error}}</p>
			@endforeach
		</div>
	@endif

	{{$slot}}

	@csrf
	<input type="hidden" name="_method" value="{{$http_method}}">
	<label for="name">{{$label}}</label>
	<input type="text" size="45" name="name" id="name" value="{{ old('name', $category->name) }}" required="true">
	<input class="btn btn-submit"  type="submit" value="{{$submit_value}}">
</form>
