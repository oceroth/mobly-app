@extends('shared.layout')
@section('content')
<div class="flex-center position-ref full-height">


    <div class="content">
        <div class="title m-b-md">
            {{$category->name}}
        </div>

        <div class="form">
            @component("categories.components.form", ['category'=>$category])
                
                @slot('http_method')
                    POST
                @endslot

                @slot('label')
                    nova categoria
                @endslot

                @slot('submit_value')
                    salvar
                @endslot

                @slot('action')
                    /categories/update/{{$category->id}}
                @endslot
            @endcomponent
        </div>
        <a href="{{ url()->previous() }}">voltar</a>
    </div>
</div> 
@endsection