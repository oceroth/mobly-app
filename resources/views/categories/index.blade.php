@extends('shared.layout')
@section('content')
<div class="floating-bar">
    <a href="{{ action('CategoryController@create')}}"> <strong class="danger">➕</strong> Novo</a>
</div>
<div class="flex-center position-ref full-height">


    <div class="content">
        <div class="title m-b-md">
            Categorias
        </div>

        <div class="links">

            @foreach ($categories as $c)
                <p>
                    <a href={{ action('CategoryController@edit',['category'=>$c->id]) }} title="Editar categoria">{{$c->name}}</a>
                    <a href="#" title="excluir categoria" @click="deleteItem({{$c->id}})"> <strong class="danger">X</strong>  excluir </a>
                    <form action="{{ action('CategoryController@destroy',['category'=>$c->id])}}" id=deleteCategory{{$c->id}} method="post">
                        @csrf
                        @method('delete')
                    </form>
                </p>
            @endforeach
        </div>
    </div>
</div>
@endsection