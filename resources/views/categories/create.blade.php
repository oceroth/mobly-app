@extends('shared.layout')
@section('content')
<div class="flex-center position-ref full-height">


    <div class="content">
        <div class="title m-b-md">
            Adicionar categoria
        </div>

        <div class="form">
            @component('categories.components.form', ['category'=>$category])
            
                @slot('http_method')
                    POST
                @endslot

                @slot('label')
                    nome da categoria
                @endslot

                @slot('submit_value')
                    adicionar
                @endslot

                @slot('action')
                    /categories/store
                @endslot
            @endcomponent
        </div>
        <a href="{{ url()->previous() }}">voltar</a>
    </div>
</div>
@endsection