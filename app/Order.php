<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function buyer(){
    	return $this->hasOne('App\Buyer');
    }

    //
    public function address(){
    	return $this->hasOne('App\ShipAdress');
    }

    public function products(){
    	return $this->belongsToMany('App\Product', 'order_items')
    				->withPivot('quantity');
    }


}
