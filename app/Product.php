<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // Retrieve the categories this product belongs
    public function categories() {

    	return $this->belongsToMany('App\Category', 'product_categories');
    }

    public function details() {
    	return $this->belongsToMany('App\Detail', 'product_details')
    				->withPivot('value');
    }

    public function orders() {
    	return $this->belongsToMany('App\Order', 'order_items')
    				->withPivot('quantity');
    }

    public static function search (string $query) {
        return self::where('name', 'like', "%{$query}%")
                ->orWhere('description', 'like', "%{$query}%")
                ->get();
    }
}
