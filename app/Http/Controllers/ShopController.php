<?php

namespace App\Http\Controllers;

use App\Product as Product;
use App\Category as Category;
use App\Order as Order;
use App\Buyer as Buyer;
use App\ShipAdress as ShipAdress;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // The idea behind these queries is to present two proposed ideas of products to highlight, presented in one 'line' of product cards. In this case it was the "sofa" category and a promotional category
    	$promo1 = Category::first()->products()->limit(3)->get();
    	$promo2 = Category::find(15)->products()->limit(3)->get();
        return view('shop.index',compact('promo1','promo2'));
    }

    public function category(Category $category) {
    	//
    	$category->loadMissing('products');

    	return view('shop.category',compact('category'));
    }

    public function product(Product $product) {
    	//
    	$product->loadMissing('details');

        return view('shop.product',compact('product'));
        //return response()->json(compact('product'));
    }

    public function cart(Request $request) {
        $cartItems = collect(session('cartItems'));
        $toBeOrdered = Product::findOrFail($cartItems->keys());
        $total = 0;
        foreach ($toBeOrdered as $item) {
            $quantity = 1 * $cartItems[$item->id];
            $total += $quantity * $item->price;
        }
        $total = money_format('%i', $total);
        return view('shop.cart',compact('cartItems','toBeOrdered','total'));
        //return response()->json(compact('cartItems','toBeOrdered'));
    }

    public function addToCart(Request $request, $productId) {
        // takes quantity from quantity input. assumes at least one
        $cartItems = collect(session('cartItems'));
        $quantity = (int) $request->input('quantity');
        $cartItems[$productId] = $quantity + $cartItems->pull($productId);        
        $request->session()->put('cartItems',$cartItems);
        return redirect(action('ShopController@cart'));
    }

    public function removeFromCart(Request $request, $productId) {
        // takes quantity from quantity input. assumes at least one
        $cartItems = collect(session('cartItems'));
        $quantity = (int) $request->input('quantity');
        $newAmount = $cartItems->pull($productId) - $quantity;
        if ($newAmount > 0) {
            $cartItems[$productId] = $newAmount;
        }        
        $request->session()->put('cartItems',$cartItems);
        return redirect(action('ShopController@cart'));
    }

    public function checkout(Request $request) {
        $cartItems = collect(session('cartItems'));
        $valid = $request->validate([
            'name' => 'bail|required|max:120|min:10',
            'cpf' => 'required|regex:/\d+/|min:11|max:11',
            'address' => 'required|min:10|max:180',
            'number' => 'required|min:1|max:7',
            'secondary_address' => 'nullable|max:20',
            'neighborhood' => 'required|max:120',
            'city' => 'required|max:120',
            'state' => 'required|max:30',    
            'postcode' => 'required|max:9|regex:/\d+/'
        ]);
        if($valid) {
            $order = new Order;
            $order->save();
            $order->buyer()->create([
                'name' => $request->input('name'),
                'cpf' => $request->input('cpf'),
            ]);
            $order->address()->create([
                'address' => $request->input('address'),
                'number' => $request->input('number'),
                'secondary_address' => $request->input('secondary_address'),
                'neighborhood' => $request->input('neighborhood'),
                'city' => $request->input('city'),
                'state' => $request->input('state'),    
                'postcode' => $request->input('postcode'),
            ]);
            foreach ($cartItems as $productId => $quantity) {
                $order->products()->attach($productId, ['quantity'=>$quantity]);
            }
            $request->session()->put('cartItems',[]);
        }
        return redirect(url('/order'));
    }

}
