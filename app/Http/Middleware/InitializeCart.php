<?php

namespace App\Http\Middleware;

use Closure;

class InitializeCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // This middleware initaialize the shopping cart
        if(!$request->session()->exists('cartItems')) {
            $request->session()->put('cartItems',[]);
        }
        return $next($request);
    }
}
