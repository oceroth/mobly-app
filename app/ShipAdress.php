<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipAdress extends Model
{
    //

    protected $fillable = ['address','number','secondary_address','neighborhood','city','state','postcode'];
}
