<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    //
    public function product() {
    	return belongsTo('App\Product');
    } 

    public function order() {
    	return belongsTo('App\Order');
    }
}
