<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        View::composer('shared.components.category-menu', function($view){
            $menuItems = \App\Category::all()->mapWithKeys(function($category){
                return [ $category->id => $category->name ];
            });
            $view->with('menuItems', $menuItems);
        });   
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //sets faker locale
        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('pt_BR');
        });
    }
}
