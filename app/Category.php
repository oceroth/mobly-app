<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Retrieve the related Products
    public function products() {
    	return $this->belongsToMany('App\Product','product_categories');
    }

}
