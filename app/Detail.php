<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    //
    public function Products() {
    	return belongsToMany('App\Product', 'product_details')->withPivot('value');
    }
}
